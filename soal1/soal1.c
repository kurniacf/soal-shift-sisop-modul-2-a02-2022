#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <wait.h>
#include <time.h>
#include <json-c/json.h>
#include <dirent.h>

typedef unsigned char bool;
static const bool false = 0;
static const bool true = 1;

// WEAPON GLOBAL INITIATE
char file_weapons[150][150];
int sum_weapons = 0;

// CHARACTERS GLOBAL INITIATE
char file_characters[150][150];
int sum_characters = 0;

struct time_management
{
    int year;
    int month;
    int day;
    int hour;
    int minute;
};


// Fungsi untuk download file dari drive
void download_file(char *url, char *path)
{
    pid_t child_id = fork();    // pid_t = Tipe data ID proses (signed integer)
    if (child_id == 0)
    {
        char *url_new = malloc(128 * sizeof(char));   // Memberikan blok memori dalam byte -stdlib
        sprintf(url_new, "https://drive.google.com/uc?id=%s&export=download", url); // printf tapi simpan di buffer
        printf("Downloading file : %s\n", url);
        execl("/usr/bin/wget", "/user/bin/wget", "--no-check-certificate", url_new, "-O", path, NULL);
        free(url_new);
        exit(0);
    } else 
    {
        int status = 0;
        waitpid(child_id, &status, 0);
    }
}

// Fungsi untuk unzip file yang telah didownload
void unzip_file(char *path)
{
    pid_t child_id = fork();
    if(child_id == 0)
    {
        printf("Unzip file : %s\n", path);
        execl("/usr/bin/unzip", "/usr/bin/unzip", path, NULL);
        exit(0);
    } else 
    {
        int status = 0;
        waitpid (child_id, &status, 0);
    }
}

// Fungsi membuat folder/directory
void create_folder(char *path)
{
    pid_t child_id = fork();
    if(child_id == 0)
    {
        printf("Create folder : %s\n", path);
        remove(path);
        execl("/bin/mkdir", "/bin/mkdir", path, NULL);
        exit(0);
    } else 
    {
        int status = 0;
        waitpid (child_id, &status, 0);
    }
}

// Fungsi membuat folder gacha
void create_folder_gacha(char *path)
{
    pid_t child_id = fork();
    if(child_id == 0)
    {
        char name[100];
        strcpy(name, "gacha_gacha");
        strcat(name, "/");
        strcat(name, path);
        execl("/bin/mkdir", "/bin/mkdir", path, NULL);
        exit(0);
    } else 
    {
        int status = 0;
        waitpid (child_id, &status, 0);
    }
}

// Fungsi untuk menghitung jumlah weapons di folder weapons
void count_weapons()
{
    DIR *dir;
    struct dirent *weapon;    //dirent.h library
    dir = opendir("weapons");
    weapon = readdir(dir);
    while (weapon != NULL)
    {
        strcpy(file_weapons[sum_weapons], weapon->d_name);
        sum_weapons = sum_weapons + 1;
    }
    closedir(dir);
}

// Fungsi untuk menghitung jumlah characters di folder characters
void count_characters()
{
    DIR *dir;
    struct dirent *character;    //dirent.h library
    dir = opendir("characters");
    character = readdir(dir);
    while (character != NULL)
    {
        strcpy(file_characters[sum_characters], character->d_name);
        sum_characters = sum_characters + 1;
    }
    closedir(dir);
}

// Fungsi Untuk gacha Characters
void characters_gacha(int random_pick, int sum_of_gacha, int remaining_primogem, char *folder_name, char *file_name)
{
    FILE *file;
    char buffer[900000];

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    char dir[150] = "characters/";
    strcat(dir, file_characters[random_pick]);

    file = fopen(dir, "r");
    fread(buffer, 900000, 1, file);
    fclose(file);
    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    char write_dir[150];
    strcpy(write_dir, "gacha_gacha");
    strcat(write_dir, "/");
    strcat(write_dir, folder_name);
    strcat(write_dir, "/");
    strcat(write_dir, file_name);
    strcat(write_dir, ".txt");

    FILE *txt = fopen(write_dir, "a");
    fprintf(txt, "$d_characters_%d_%s_%d\n", sum_of_gacha, json_object_get_int(rarity), json_object_get_string(name), remaining_primogem);
    fclose(txt);
}

void weapons_gacha(int random_pick, int sum_of_gacha, int remaining_primogem, char *folder_name, char *file_name)
{
    FILE *file;
    char buffer[900000];

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    char dir[150] = "weapons/";
    strcat(dir, file_weapons[random_pick]);

    file = fopen(dir, "r");
    fread(buffer, 900000, 1, file);
    fclose(file);
    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    char write_dir[150];
    strcpy(write_dir, "gacha_gacha");
    strcat(write_dir, "/");
    strcat(write_dir, folder_name);
    strcat(write_dir, "/");
    strcat(write_dir, file_name);
    strcat(write_dir, ".txt");

    FILE *txt = fopen(write_dir, "a");
    fprintf(txt, "$d_weapons%d_%s_%d\n", sum_of_gacha, json_object_get_int(rarity), json_object_get_string(name), remaining_primogem);
    fclose(txt);
}

int main()
{
    // Download dari drive
    // characters: https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view
    // weapons: https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view
    download_file("1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "characters.zip");
    download_file("1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "weapons.zip");

    // Unzip File Zip
    unzip_file("characters.zip");
    unzip_file("weapons.zip");

    // Create Folder "gacha_gacha"
    create_folder("gacha_gacha");

    chdir("gacha_gacha");

    // Hitung Jumlah File
    count_weapons();
    count_characters();

    while(true)
    {
        sleep(1);
        time_t time_now;
        struct stime *time_nows;
        time_now = time(NULL);

        // Waktu
        struct time_management start_time;
        struct time_management finish_time;

        // Waktu saat 30 March 2022 Jam 04:44
        start_time.year = 2022;
        start_time.month = 3;
        start_time.day = 30;
        start_time.hour = 4;
        start_time.minute = 44;

        // Waktu saat berakhir gacha 30 March 2022 jam 07:44 (3 jam setelahnya)
        finish_time.year = 2022;
        finish_time.month = 3;
        finish_time.day = 30;
        finish_time.hour = 7;
        finish_time.minute = 44;

        // Untuk Start Gacha
        if(
        localtime(&time_now)->tm_year == start_time.year && 
        localtime(&time_now)->tm_mon == start_time.month &&
        localtime(&time_now)->tm_hour == start_time.hour && 
        localtime(&time_now)->tm_mday == start_time.day && 
        localtime(&time_now)->tm_hour == start_time.hour &&
        localtime(&time_now)->tm_min == start_time.minute
        ) {
        pid_t child_id = fork();

        if(child_id == 0)
        {
            int status = 1;
            pid_t child_id_start = fork();

            if(child_id_start == 0)
            {
            char folder_name[150];
            char file_name[150];
            int num_of_folder = 0;
            int num_of_file = 0;
            int primogems = 79000;

            int index = 1;
            while(primogems >= 160)
            {
                primogems = primogems - 160;

                // Di Mod 10
                if (index % 10 == 1)
                {
                num_of_file = num_of_file + 10;
                snprintf(file_name, 150, "%d:%d:%d_gacha_%d", 
                    localtime(&time_now)->tm_hour,
                    localtime(&time_now)->tm_min,
                    localtime(&time_now)->tm_sec,
                    num_of_file
                );
                } 

                // Di Mod 90
                if (index % 90 == 1)
                {
                num_of_folder = num_of_folder + 90;
                snprintf(folder_name, 150, "total_gacha_%d", folder_name);
                create_folder_gacha(folder_name);
                }

                // Characters Gacha
                if(index % 2 == 1)
                {
                int random_gacha = rand() % sum_characters;
                characters_gacha(random_gacha, index, primogems, folder_name, file_name);
                } else if (index % 2 == 0)
                {
                int random_gacha = rand() % sum_weapons;
                weapons_gacha(random_gacha, index, primogems, folder_name, file_name);
                }
                index = index + 1;
            }

            } else 
            {
            int status = 0;
            waitpid (child_id_start, &status, 0);
            }

        } else 
        {
            int status = 0;
            waitpid (child_id, &status, 0);
        }
        }

        // Untuk Finish Gacha
        if(
        localtime(&time_now)->tm_year == finish_time.year && 
        localtime(&time_now)->tm_mon == finish_time.month &&
        localtime(&time_now)->tm_hour == finish_time.hour && 
        localtime(&time_now)->tm_mday == finish_time.day && 
        localtime(&time_now)->tm_hour == finish_time.hour &&
        localtime(&time_now)->tm_min == finish_time.minute
        ) {
        pid_t child_id_finish = fork();
        int status;

        if(child_id_finish == 0)
        {
            printf("Zip file : \n");
            char *path[] = {"zip", "-p", "satuduatiga", "-mr", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
            execv("/usr/bin/zip", path);
            exit(0);
        } else 
        {
            int status = 0;
            waitpid (child_id_finish, &status, 0);
        }
        }
    }

    // char type_characters[10] = "characters";
    // char type_weapons[10] = "weapons";
    // check_gacha(type_characters);
}