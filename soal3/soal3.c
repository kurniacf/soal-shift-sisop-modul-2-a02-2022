#include <string.h>
#include <syslog.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
 
void writeLog() {
 FILE *file = fopen("/home/amanda/modul2/air/list.txt", "a");
 
 DIR *directory;
 struct dirent *direntry;
 
 directory = opendir("/home/amanda/modul2/air");
 
 while(direntry = readdir(directory)) {
  char userID[200] = "";
  char filePermission[5] = "";
  struct stat fs;
  char templateList[500] = "";
  char path[50] = "/home/amanda/modul2/air/";
 
  cuserid(userID);
  strcat(path, direntry->d_name);
 
  int r = stat(path, &fs);
 
  if(!(S_ISREG(fs.st_mode) && strcmp(direntry->d_name, "list.txt")!=0)) continue;
 
  if(fs.st_mode & S_IRUSR) strcat(filePermission, "r");
  if(fs.st_mode & S_IWUSR) strcat(filePermission, "w");
  if(fs.st_mode & S_IXUSR) strcat(filePermission, "x");
 
  sprintf(templateList, "%s_%s_%s\n", userID, filePermission, direntry->d_name);
  fprintf(file, templateList);
 }
 
fclose(file);
}
 
int main(){
int status1, status2, status3, status4;
pid_t pid1, pid2, pid3, pid4, pid5, pid6, pid7, pid8, pid9, pid10, pid11;
pid1 = fork();
 
if (pid1 < 0) {
 exit(EXIT_FAILURE);
}
 
if (pid1 == 0) {
 char *argv[] = {"mkdir", "-p", "/home/amanda/modul2/darat", NULL};
 execv("/bin/mkdir", argv);
} 
else {
 pid2 = fork();
 if (pid2 == 0) {
  sleep(3); 
  char *argv[] = {"mkdir", "-p", "/home/amanda/modul2/air", NULL};
  execv("/bin/mkdir", argv);
 }
 else {
  pid3 = fork();
  if (pid3 == 0) {
   char *argv[] = {"unzip", "-q", "/home/amanda/Downloads/animal.zip", "-d", "/home/amanda/modul2", NULL};
   execv("/usr/bin/unzip", argv);
  }
  else {
   while((wait(&status1)) > 0);
   pid4 = fork(); 
   if (pid4 == 0) {
    char *arg[]={"find","/home/amanda/modul2/animal","-name","*_darat.jpg","-exec","mv","{}","/home/amanda/modul2/darat",";",NULL};
    execv("/usr/bin/find",arg);
   }
   else {
    pid5 = fork();
    if (pid5 == 0) {
     char *arg[]={"find","/home/amanda/modul2/animal","-name","darat_*.jpg","-exec","mv","{}","/home/amanda/modul2/darat",";",NULL};
     execv("/usr/bin/find",arg);
    }
    else {
     pid6 = fork();
     if (pid6 == 0) {
      char *arg[]={"find","/home/amanda/modul2/animal","-name","*_darat_*.jpg","-exec","mv","{}","/home/amanda/modul2/darat",";",NULL};
      execv("/usr/bin/find",arg);
     } 
     else {
      pid7 = fork();
      if (pid7 == 0) {
       char *arg[]={"find","/home/amanda/modul2/animal","-name","*_air.jpg","-exec","mv","{}","/home/amanda/modul2/air",";",NULL};
       execv("/usr/bin/find",arg);
      } 
      else {
       pid8 = fork();
       if(pid8 == 0) {
        char *arg[]={"find","/home/amanda/modul2/animal","-name","*_air_*.jpg","-exec","mv","{}","/home/amanda/modul2/air",";",NULL};
        execv("/usr/bin/find",arg);
       }
        else {
         while((wait(&status2)) > 0);
         pid9 = fork();
         if(pid9 == 0) {
          char *arg[]={"find","/home/amanda/modul2/animal","-name","*.jpg","-exec","rm","{}",";",NULL};
          execv("/usr/bin/find",arg); 
         } 
         else {
          while((wait(&status3)) > 0);
          pid10 = fork(); 
          if(pid10 == 0) {
           char *arg[]={"find","/home/amanda/modul2/darat","-name","*_bird_*.jpg","-exec","rm","{}",";",NULL};
           execv("/usr/bin/find",arg); 
          }
          else {
           pid11 = fork();
           if(pid11 == 0) {
            char *arg[]={"find","/home/amanda/modul2/darat","-name","*_bird.jpg","-exec","rm","{}",";",NULL};
            execv("/usr/bin/find",arg); 
           }
           else {
            while((wait(&status4)) > 0);
            writeLog();
           }
          }
         } 
        }
       } 
      }
     }
    }
   }
  }
 }
}