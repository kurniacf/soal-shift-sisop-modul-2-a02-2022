#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

void removeSubstr (char *string) {
    char *match;
    while ((match = strstr(string, ".png"))) {
        *match = '\0';
        strcat(string, match+4);
    }
}

void unzipfiles()
	{
		char *argv[] = {"unzip",  "dramkor/drakor.zip","-d","/home/avind/shift2/drakor/","*.png", NULL};
		execv("/bin/unzip", argv);
		exit(0);
	}
	
void makeFold(char* kategori)
{
	char Dir[100] = "/home/avind/shift2/drakor/";
	strcat(Dir, kategori);
	pid_t child_id;
	int status;
	DIR* dir = opendir(Dir);
	if (!(dir)) {
		child_id = fork();
		if (child_id < 0) {
			exit(EXIT_FAILURE);
		}
		if (child_id == 0) {
		char *argv[] = {"mkdir",  Dir, NULL};
		execv("/bin/mkdir", argv);
		exit(0);
		}
		while ((wait(&status)) > 0);
		
	}
	
	closedir(dir);

}	

void movePos(char* Pos, char* fileName)
{
	char detil[3][50];
	int i=0;
	puts (fileName);
	char* tempName= (char*)malloc(50);
	strcpy(tempName, Pos);
	removeSubstr(tempName);
	puts (tempName);	
	char* token = strtok(tempName, ";");

	while (token != NULL) {
		strcpy(detil[i], token);
		token = strtok(NULL, ";");
		i++;
		}
	
	makeFold(detil[2]);
	for(i=0; i<3; i++)
	{
//		puts (detil[i]);
	}
}
	

void managePos()
{
	DIR *dp;
		  struct dirent *ep;
		  dp = opendir("/home/avind/shift2/drakor");
	    while ((ep = readdir (dp))) 
	    {
	    
	    if (strstr(ep->d_name, ".png"))
			  {
			  	if (strstr(ep->d_name, "_")!=0) {
					char* tempdob= (char*)malloc(50);
					strcpy(tempdob, ep->d_name);
					char* token;
					char* rest = tempdob;

					while ((token = strtok_r(rest, "_", &rest)))
						{
							movePos(token, ep->d_name);
							//puts (ep->d_name);
						}
					}
				else
				{
				
					movePos(ep->d_name, ep->d_name);
				}
			  }
	    }

	    (void) closedir (dp);
}

int main() {
	pid_t child_id;
	int status;

	child_id = fork();
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
		char *argv[] = {"mkdir","-p","/home/avind/shift2/drakor", NULL};
		execv("/bin/mkdir", argv);
	}
	while ((wait(&status)) > 0);

	//child pertama
	child_id = fork();
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
		unzipfiles();
	}
	//tunggu child pertama
	while ((wait(&status)) > 0);

	//child kedua
	child_id = fork();
	if (child_id < 0) {
		exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
		managePos();
		exit(0);
	}
	//tunggu child kedua
	while ((wait(&status)) > 0);  
}
