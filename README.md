# Sistem Operasi 2022
Kelas Sistem Operasi A - Kelompok A02
### Nama Anggota 
- Kurnia Cahya Febryanto (5025201073)
- Amanda Salwa Salsabila (5025201172)
- Avind Pramana Azhari (05111940000226)

## Shift 2
### Soal 1
Mas Refadi adalah seorang wibu gemink dan game favoritnya adalah bengshin impek dengan ada sistem gacha item yang membuat orang-orang selalu ketagihan. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut.
#### 1-a
##### soal
Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).
##### Penjelasan
###### 1. Download File 

</br>

Langkah pertama yaitu membuat fungsi untuk mendowload file. Disini fungsinya bernama `download_file`. Awalnya membuat fork dengan inisialisasi child_id berupa tipe data ID Proses yaitu `pid_t` dan dilakukan check jika == 0, maka akan menjalankan fungsi downloadnya. Selanjutnya adalah mendeklarasikan variabel pointer dengan alokasi memori seukuran `128*sizeof(char)` url_new sebagai gabungan url drive google dengan link filenya. 
```
char *url_new = malloc(128 * sizeof(char))
```
lalu digabungkan string antara url drive google dengan link file seperti berikut
```
sprintf(url_new, "https://drive.google.com/uc?id=%s&export=download", url)
```
Setelah itu dilakukan download menggunakan execl dengan memakai bantuan dari wget yang bisa diakses sebagai berikut
```
execl("/usr/bin/wget", "/user/bin/wget", "--no-check-certificate", url_new, "-O", path, NULL)
```
lalu kosongkan memori agar bisa digunakan lagi dengan free dan keluar program dengan exit
```
free(url_new)
exit(0)
```
Untuk menangkap error atau kesalahan, maka gunakan `waitpid` agar dapat menangguhkan proses pemanggilan sampai sistem mendapatkan informasi child_id.

</br>

###### 2. Ekstrak File (Unzip) 

</br>

Langkah kedua yaitu ekstrak file dengan membuat fungsi `unzip_file`. Proses unzip dilakukan dengan inisialiasi fork terlebih dahulu dan dilakukan pengecekan
```
pid_t child_id = fork()
```
Selanjutnya menggunakan execl, kita unzip dengan bantuan library unzip serta dengan memasukkan patch nama file yang mau di unzip
```
execl("/usr/bin/unzip", "/usr/bin/unzip", path, NULL)
```
Setelah itu, keluar dari program dengan `exit` dan beri `waitpid` untuk kondisi yang berbeda

</br>

###### 3. Create Folder 

</br>

Pada langkah ketiga ini yaitu membuat folder baru bernamakan `gacha_gacha`. Buat fungsi `create_folder` dan lakukan inisialisasi fork
```
 pid_t child_id = fork()
```
Setelah itu, dilakukan check apakah folder sudah pernah dibuat atau belum dengan menggunakan `remove()` untuk menghapus folder yang sudah ada dibuat terlebih dahulu.
```
remove(path)
```
Selanjutnya, menggunakan `execl` yaitu melakukan make directory dengan mkdir seperti berikut
```
execl("/bin/mkdir", "/bin/mkdir", path, NULL)
```
Lalu keluar dari program dan beri `waitpid` untuk kondisi yang berbeda

</br>

#### 1-b
##### soal
Pada soal, setiap kali gacha maka item characters dan item weapon akan selalu bergantian diambil datanya dari database. Oleh karena itu,  setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut. Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA
##### Penjelasan
Pertama dilakukan inisialisasi dengan child_id fork() dan juga buat variabel index sebagai menghitung jumlah gachanya. Setelah itu dilakukan pengecekan. Jika di mod 10 maka
```
 if (index % 10 == 1){
     num_of_file = num_of_file + 10;
 }
```
Ketika di mod 10, maka nilai dari jumlah filenya akan bertambah 10 setelah itu dilakukan snprintf untuk nama filenya(penjelasan 1-c). Jika di mod 90, maka 
```
if (index % 90 == 1){
    num_of_folder = num_of_folder + 90;
}
```
Ketika di mod 90, maka nilai dari jumlah file dalam folder akan bertambah menjadi 90, dan dilakukan snprintf untuk penamaan file (penjelasan 1-c) dan membuat folder gacha baru di fungsi `create_folder_gacha `
```
char name[100];
strcpy(name, "gacha_gacha");
strcat(name, "/");
strcat(name, path);
execl("/bin/mkdir", "/bin/mkdir", path, NULL);
```

</br>

#### 1-c
##### soal
Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.
##### Penjelasan
Pada soal diminta untuk menentukan waktu pelaksanaan gacha. Maka langkah awal dari membuat struct dari waktunya terlebih dahulu 
```
struct time_management
{
    int year;
    int month;
    int day;
    int hour;
    int minute;
};
```
Setelah itu inisialisasi waktunya dengan struct tersebut dan dibuat waktu mulai dan akhir sesuai ketentuan di soal. 
```
start_time.year = 2022;
start_time.month = 3;
start_time.day = 30;
start_time.hour = 4;
start_time.minute = 44;

finish_time.year = 2022;
finish_time.month = 3;
finish_time.day = 30;
finish_time.hour = 7;
finish_time.minute = 44;
```
Lalu cara kerja program akan dibagi berdasarkan waktu mulai dan waktu berakhir menggunakan if else. Pada soal juga diminta untuk melakukan penamaan file dan folder dari hasil yang telah dilakukan di 1-b. Untuk di mod 10, maka penamaan filenya akan dilakukan sebagai berikut. 
```
snprintf(file_name, 150, "%d:%d:%d_gacha_%d", 
        localtime(&time_now)->tm_hour,
        localtime(&time_now)->tm_min,
        localtime(&time_now)->tm_sec,
        num_of_file
);
```
Penamaan diatas menggabungkan localtime dari jam, menit, dan detik, serta jumlah filenya. Selanjutnya untuk di mod 90, maka untuk melakukan penamaan file adalah sebagai berikut
```
snprintf(folder_name, 150, "total_gacha_%d", folder_name);
```
Penamaan diatas menggabungkan dengan folder name yang ada

</br>

#### 1-d
##### soal
Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880
##### Penjelasan
Pada soal ini adalah melakukan proses gacha. Proses dilakukan saat gacha telah di start. Sesuai dengan yang ada di 1-b, maka untuk gacha ganjil akan meng-gacha characters dan genap akan meng-gacha weapons
```
if(index % 2 == 1)
{
    // characters
} else if (index % 2 == 0)
{
    // weapons
}
```
Setelah itu, membuat fungsi random_pick dengan cara memodulo rand() dengan jumlah keseluruhan data di folder characters ataupun di weapons. Setelah itu membuat fungsi gacha nya. Awalnya melakukan struct JSON sesuai dengan refrensi di soal
```
    FILE *file;
    char buffer[900000];

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    char dir[150] = "characters/";
    strcat(dir, file_characters[random_pick]);

    file = fopen(dir, "r");
    fread(buffer, 900000, 1, file);
    fclose(file);
    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

```
Kemudian tinggal membuat folder dan file dari hasil perhitungan tersebut menjadi file.txt. Setelah itu, jangan lupa format penamaan disesuaikan dengan soal. Hal ini juga berlaku kepada folder weapons. Sehingga hasil akhirnya adalah file-file.txt hasil dari gacha


</br>

#### 1-d
##### soal
Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44. Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)
##### Penjelasan
Pada bagian ini diminta untuk menjadikan hasil gacha tadi menjadi folder lalu di zip dengan password. Hal ini dilakukan saat format waktu struct tadi pada bagian finish_time. Langkahnya yang dilakukan yaitu melakukan zip dengan password
```
char *path[] = {"zip", "-p", "satuduatiga", "-mr", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
execv("/usr/bin/zip", path);
```
Setelah itu, folder akan menjadi zip dengan enkripsi password

</br>

### Soal 2
Pada soal ini, file zip yang bernama drakor.zip akan diextrack menggunakan script berbahasa C. Isi file yang diambil yaitu hanya berupa file dengan extension png saja. Pada setiap file png tersebut terdapat informasi mengenai judul, tahun, dan genre dari drakor. Informasi tersebut mempunyai format judul;tahun;genre.png, akan tetapi juga terdapat gambar yang mempunyai 2 informasi drakor yang dipisahkan mengguanakan "_". Pertama, kita memerlukan sebuah directory untuk meletakkan gambar yang akan diekstrak. yaitu menggunakan :
```
char *argv[] = {"mkdir","-p","/home/avind/shift2/drakor", NULL};
execv("/bin/mkdir", argv);
```
kode tersebut akan dijalankan menggunakan fork agar script dapat berjalan berurutan. Kemudian file zip akan di unzip menggunakan sebuah fungsi
```
void unzipfiles()
	{
		char *argv[] = {"unzip",  "dramkor/drakor.zip","-d","/home/avind/shift2/drakor/","*.png", NULL};
		execv("/bin/unzip", argv);
	}
```
Fungsi tersebut juga akan dijalankan mengguanakan fork(), untuk direktori ekstrak yaitu `home/avind/shift2/drakor/` yang telah dibuat sebelumnya. Setelah itu, setiap file akan dicek namanya menggunakan dirent yang berada pada fungsi berikut
```
void managePos()
{
	DIR *dp;
	struct dirent *ep;
	dp = opendir("/home/avind/shift2/drakor");
	while ((ep = readdir (dp))) 
	{

	if (strstr(ep->d_name, ".png")){
		if (strstr(ep->d_name, "_")!=0) {
			char* tempdob= (char*)malloc(50);
			strcpy(tempdob, ep->d_name);
			char* token;
			char* rest = tempdob;

			while ((token = strtok_r(rest, "_", &rest)))
				{
					movePos(token, ep->d_name);
					//puts (ep->d_name);
				}
			}
		else
		{

			movePos(ep->d_name, ep->d_name);
		}
	}
	}

	(void) closedir (dp);
}
```
Pada fungsi ini, setiap file akan dicek apabila terdapat karakter "_" didalamnya. Jika ada, maka nama file akan dijadikan 2 buah instance berbeda dengan menggunakan strtok, dan dimasukkan kedalam fungsi movePos. Jika tidak terdapat, maka akan langsung dimasukkan ke fungsi movePos. Fungsi movePos adalah sebagai berikut
```
void movePos(char* Pos, char* fileName)
{
	char detil[3][50];
	int i=0;
	puts (fileName);
	char* tempName= (char*)malloc(50);
	strcpy(tempName, Pos);
	removeSubstr(tempName);
	puts (tempName);	
	char* token = strtok(tempName, ";");

	while (token != NULL) {
		strcpy(detil[i], token);
		token = strtok(NULL, ";");
		i++;
		}
	
	makeFold(detil[2]);
	for(i=0; i<3; i++)
	{
//		puts (detil[i]);
	}
}
```
Pada fungsi ini, judul yang sudah diambil akan dibagi menjadi 3 buah data yaitu detil[0] hingga detil[2] yang akan diisi oleh judul, tahun, dan genre drakornya. Karena pada nama file masih terdapat ".png" didalam namanya, maka akan digunakan fungsi removeSubstr() yang berisikan
```
void removeSubstr (char *string) {
    char *match;
    while ((match = strstr(string, ".png"))) {
        *match = '\0';
        strcat(string, match+4);
    }
}
```
Setelah didapatkan data dari tiap file, akan dijalankan fungsi makeFold() yang berfungi untuk membuat folder menurut genre drakor yang bersangkutan jika belum tersedia. Isi dari makeFold() yaitu
```
void makeFold(char* kategori)
{
	char Dir[100] = "/home/avind/shift2/drakor/";
	strcat(Dir, kategori);
	pid_t child_id;
	int status;
	DIR* dir = opendir(Dir);
	if (!(dir)) {
		child_id = fork();
		if (child_id < 0) {
			exit(EXIT_FAILURE);
		}
		if (child_id == 0) {
		char *argv[] = {"mkdir",  Dir, NULL};
		execv("/bin/mkdir", argv);
		exit(0);
		}
		while ((wait(&status)) > 0);
	}
	closedir(dir);
}	
```
Jika folder genre sudah dibuat, maka file gambar drakor tersebut dapat dipindahkan ke folder genrenya masing masing.


### Soal 3
Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang
#### 3-a
##### Soal
Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke-2 dengan nama “air” 
##### Penjelasan
Pembuatan directory dilakukan dengan menggunakan `execv` dengan perintah mkdir 
```
char *argv[] = {"mkdir", "-p", "/home/amanda/modul2/darat", NULL};
execv("/bin/mkdir", argv);
```
Karena diperlukan jeda/selang waktu selama 3 detik dalam pembuatan directory ke-2, maka diberi `sleep(3)`
Kemudian, directory ke-2 dibuat menggunakan `execv` dengan perintah mkdir 
```
char *argv[] = {"mkdir", "-p", "/home/amanda/modul2/air", NULL};
execv("/bin/mkdir", argv);
```

#### 3-b
##### Soal
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”
##### Penjelasan
animal.zip diextract menggunakan `execv` dengan perintah unzip
```
char *argv[] = {"unzip", "-q", "/home/amanda/Downloads/animal.zip", "-d", "/home/amanda/modul2", NULL};
execv("/usr/bin/unzip", argv);
```

#### 3-c
##### Soal
Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus
##### Penjelasan
Hasil extract file berupa file-file .jpg yang beberapa diantaranya memiliki keterangan 'darat' dan 'air' pada nama filenya, sesuai dengan keterangan hewan masing-masing file. Kita perlu untuk melakukan klasifikasi terhadap file-file tersebut. File hewan darat dimasukan ke folder 'darat' dan file hewan air dimasukan ke folder 'air'. Untuk file yang tidak memiliki keterangan, file harus dihapus

Klasifikasi file hewan darat dilakukan dengan melakukan `execv` menggunakan perintah `find` path `-name *_darat.jpg/darat_*.jpg/*_darat_*.jpg -exec mv {}` destination path
```
char *arg[]={"find","/home/amanda/modul2/animal","-name","*_darat.jpg","-exec","mv","{}","/home/amanda/modul2/darat",";",NULL};
execv("/usr/bin/find",arg);
```
```
char *arg[]={"find","/home/amanda/modul2/animal","-name","darat_*.jpg","-exec","mv","{}","/home/amanda/modul2/darat",";",NULL};
execv("/usr/bin/find",arg);
```
```
char *arg[]={"find","/home/amanda/modul2/animal","-name","*_darat_*.jpg","-exec","mv","{}","/home/amanda/modul2/darat",";",NULL};
execv("/usr/bin/find",arg);
```

Klasifikasi file hewan darat dilakukan dengan melakukan `execv` menggunakan perintah `find` path `-name *_air.jpg/*_air_*.jpg -exec mv {}` destination path
```
char *arg[]={"find","/home/amanda/modul2/animal","-name","*_air.jpg","-exec","mv","{}","/home/amanda/modul2/air",";",NULL};
 execv("/usr/bin/find",arg);
```
```
char *arg[]={"find","/home/amanda/modul2/animal","-name","*_air_*.jpg","-exec","mv","{}","/home/amanda/modul2/air",";",NULL};
 execv("/usr/bin/find",arg);
```

Untuk penghapusan file hewan yang tidak memiliki keterangan dilakukan dengan melakukan `execv` menggunakan perintah `find` path `name *.jpg -exec rm {}`
```
char *arg[]={"find","/home/amanda/modul2/animal","-name","*.jpg","-exec","rm","{}",";",NULL};
execv("/usr/bin/find",arg); 
```

#### 3-d
##### Soal
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file
##### Penjelasan
File hewan darat yang merupakan burung ditandai dengan adanya 'bird' pada nama filenya. File-file tersebut harus dihapus dari directory 'darat'

Penghapusan file dilakukan dengan melakukan `execv` menggunakan perintah `find` path `-name *_bird*.jpg/*_bird.jpg -exec rm {}`
```
char *arg[]={"find","/home/amanda/modul2/darat","-name","*_bird_*.jpg","-exec","rm","{}",";",NULL};
execv("/usr/bin/find",arg); 
```
```
char *arg[]={"find","/home/amanda/modul2/darat","-name","*_bird.jpg","-exec","rm","{}",";",NULL};
execv("/usr/bin/find",arg); 
```

#### 3-e
##### Soal
Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png
##### Penjelasan
Conan perlu melakukan list terhadap nama-nama file yang terdapat pada directory 'air' dengan format UID_[UID file permission]_Nama File.[jpg/png] dan memasukan list tersebut kedalam list.txt

Dibuat function bantuan yang bernama writeLog dimana function ini akan membuat list.txt, melakukan list dengan mengambil user ID, permission, serta nama dari masing-masing file yang terdapat di directory 'air'. Function ini akan dipanggil di fork terakhir
```
writeLog();
```

File list.txt dibuat dan directory 'air' dibuka agar file-file di dalamnya dapat terbaca menggunakan perintah `opendir()`. Entry terhadap directory menggunakan `struct dirent` untuk membaca file-file yang ada dalam directory 'air'
```
FILE *file = fopen("/home/amanda/modul2/air/list.txt", "a");
 
DIR *directory;
struct dirent *direntry;
 
directory = opendir("/home/amanda/modul2/air");
```

Digunakan while loop untuk pembuatan list. Dalam while loop ini juga masing-masing file akan diambil user ID, permission, serta namanya:
1. user ID idambil dengan menggunakan perintah `cuserid()` dan ditampung dalam array string `userID[200]`
2. File permission dicek dengan menggunakan library `s<ys/stat.h>` dan hasil cek dicatat menggunakan perintah strcat ke dalam array string `filePermission[5]`
3. Agar nama file dapat dipisah dari alamat file, digunakan `direntry->d_name`
4. Semua array digabungkan ke dalam suatu array string bernama `templateList[500]` dengan menggunakan sprintf, sesuai dengan format list yakni `%s_%s_%s` dimana string pertama merupakan userID, string kedua merupakan filePermission, dan string ketiga merupakan direntry->d_name
```
while(direntry = readdir(directory)) {
  char userID[200] = "";
  char filePermission[5] = "";
  struct stat fs;
  char templateList[500] = "";
  char path[50] = "/home/amanda/modul2/air/";
 
  cuserid(userID);
  strcat(path, direntry->d_name);
 
  int r = stat(path, &fs);
 
  if(!(S_ISREG(fs.st_mode) && strcmp(direntry->d_name, "list.txt")!=0)) continue;
 
  if(fs.st_mode & S_IRUSR) strcat(filePermission, "r");
  if(fs.st_mode & S_IWUSR) strcat(filePermission, "w");
  if(fs.st_mode & S_IXUSR) strcat(filePermission, "x");
 
  sprintf(templateList, "%s_%s_%s\n", userID, filePermission, direntry->d_name);
  fprintf(file, templateList);
 }
```
